package edu.udel.testassist.configuration;

import java.util.ArrayList;
import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;

public class TaintInfo {
	
	private final Integer bco;
	private final ArrayList<Integer> offsets;

	@JsonCreator
	public TaintInfo(@JsonProperty("bco") Integer bco, 
			@JsonProperty("offsets") Collection<Integer> offsets) {
		this.bco = bco;
		this.offsets = Lists.newArrayList(offsets);
	}

	@JsonGetter
	public Integer bco() {
		return bco;
	}
	
	@JsonGetter
	public ArrayList<Integer> offsets() {
		return offsets;
	}
	
	@Override
	public String toString() {
		return String.format("[%d, %s]", bco, offsets);
	}
	
	@Override
	public boolean equals(Object that){
		if (that instanceof TaintInfo){
			TaintInfo ti = (TaintInfo) that;
			return this.bco.equals(ti.bco);
		}
		return false;
	}
	
	@Override
	public int hashCode(){
		if (this.offsets().isEmpty()) return this.bco.hashCode();
		return this.bco.hashCode()*42 + this.offsets().get(0).hashCode();
	}

//	@Override
//	public int compareTo(TaintInfo o) {
//		// TODO Auto-generated method stub
//		return this.bco == o.bco ? 0 : 1;
//	}
}

