package edu.udel.testassist.configuration;

import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

public class TestDB {

	public static void main(String[] args) {
		EntityManagerFactory emf =
				Persistence.createEntityManagerFactory("./db/points.odb");
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		ControlFlowInfo cfi = new ControlFlowInfo("testassist.test.Counter.getEven()I", new HashMap<Integer, Integer>());
		em.persist(cfi);
		System.out.println("haha");
		em.getTransaction().commit();
		TypedQuery<ControlFlowInfo> query =
	            em.createQuery("SELECT p FROM ControlFlowInfo p", ControlFlowInfo.class);
		
		List<ControlFlowInfo> res = query.getResultList();
		System.out.println(res);
        em.close();
        emf.close();
	}

}
