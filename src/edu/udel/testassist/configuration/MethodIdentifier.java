package edu.udel.testassist.configuration;

import java.io.Serializable;

import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

@Entity
public class MethodIdentifier implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public final String className;
	public final String methodName;
	public final String methodSignature;

	public MethodIdentifier(final String s) {
		int indexOfLastDot = s.lastIndexOf('.');
		int indexOfLParen = s.indexOf('(');

		this.className = s.substring(0, indexOfLastDot);
		this.methodName = s.substring(indexOfLastDot + 1, indexOfLParen);
		this.methodSignature = s.substring(indexOfLParen);
	}

	@JsonCreator
	public MethodIdentifier(@JsonProperty("className") final String className,
			@JsonProperty("methodName") final String methodName,
			@JsonProperty("methodSignature") final String methodSignature) {
		this.className = className;
		this.methodName = methodName;
		this.methodSignature = methodSignature;
	}

	@JsonGetter
	public String className() {
		return className;
	}

	@JsonGetter
	public String methodName() {
		return methodName;
	}

	@JsonGetter
	public String methodSignature() {
		return methodSignature;
	}

	public String toString() {
		return String.format("%s.%s%s", className, methodName, methodSignature);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(className, methodName, methodSignature);
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj instanceof MethodIdentifier) {
			final MethodIdentifier other = (MethodIdentifier) obj;
			return Objects.equal(className, other.className)
					&& Objects.equal(methodName, other.methodName)
					&& Objects.equal(methodSignature, other.methodSignature);
		} else {
			return false;
		}
	}
}