package edu.udel.testassist.configuration;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Range {

	private final int bco;
	private final int pattern;
	private final String excep;

	public Range(int line, int pattern) {
		this.bco = line;
		this.pattern = pattern;
		this.excep = "";
	}

	@JsonCreator
	public Range(@JsonProperty("bco") int line,
			@JsonProperty("pattern") int pattern,
			@JsonProperty("excep") String excep) {
		this.bco = line;
		this.pattern = pattern;
		this.excep = excep;
	}

	@JsonGetter
	public int bco() {
		return bco;
	}

	@JsonGetter
	public int pattern() {
		return pattern;
	}

	@JsonGetter
	public String excep() {
		return excep;
	}

	@Override
	public String toString() {
		return String.format("[%d, %d, %s]", bco, pattern, excep);
	}

	@Override
	public boolean equals(Object dat) {
		if (dat instanceof Range) {
			Range datrange = (Range) dat;
			return this.bco == datrange.bco;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.bco;
	}

	public boolean inRange(int i) {
		return bco == i && pattern > 0;
	}

}
