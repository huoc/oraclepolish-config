package edu.udel.testassist.configuration;

import java.io.Serializable;

import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class ControlFlowInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	public String methodIdentifier;
	private Map<Integer, List<Integer>> map;

	public ControlFlowInfo(final String identifier,
			final Map<Integer, List<Integer>> cfpairs) {

		this.methodIdentifier = identifier;
		this.map = cfpairs;
	}

	public Map<Integer, List<Integer>> getControlFlowMap() {
		return this.map;
	}

	public String identifier() {
		return this.methodIdentifier;
	}
	
	public void setMap(Map<Integer, List<Integer>> m){
		this.map = m;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof ControlFlowInfo) {
			ControlFlowInfo dat = (ControlFlowInfo) o;
			return dat.methodIdentifier.equals(methodIdentifier);
		}
		return false;
	}

	@Override
	public String toString() {
		return String.format("[%s: %s]", methodIdentifier, map);
	}
}
